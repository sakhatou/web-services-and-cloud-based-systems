from flask import Flask, request
from flask_restful import reqparse, abort, Api, Resource
import string, random, json


app = Flask(__name__)
api = Api(app)

URLS = {}

def add_url_shortener(old_url, new_url):
	URLS[new_url] = {'old_url': old_url}
	
def shorten_url():
	return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))

def abort_if_url_doesnt_exist(id):
	if id not in URLS:
		abort(404, message="error")

class MyHandler(Resource):
	def get(self):
		return list(URLS), 200
	
	def post(self):
		url = request.form.get('url')
		new_url = shorten_url()
		add_url_shortener(url, new_url)
		return new_url, 201 
	
	def delete(self):
		return '', 204


class VarHandler(Resource):
	def get(self, id):
		id_param = request.form.get('id')
		abort_if_url_doesnt_exist(id_param)
		return URLS[id_param], 301
	
	def put(self, id):
		id_param = request.form.get('id')
		abort_if_url_doesnt_exist(id_param)
		return '', 200
		
	def delete(self, id):
		id_param = request.form.get('id')
		abort_if_url_doesnt_exist(id_param)
		del URLS[id_param]		
		return '', 204
		
		
##
## Actually setup the Api resource routing here
##
api.add_resource(MyHandler, '/')
api.add_resource(VarHandler, '/<string:id>')


if __name__ == '__main__':
	app.run(debug=True)