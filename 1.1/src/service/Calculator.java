package service;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService()
public class Calculator {

  @WebMethod
  public int add(int a, int b) {
    return a + b;
  }

  @WebMethod
  public int subtract(int a, int b) {
    return a-b;
  }

  @WebMethod
  public int multiply(int a, int b) {
    return Math.multiplyExact(a,b);
  }

  @WebMethod
  public Integer divide(int a, int b) {
    return Math.floorDiv(a,b);
  }

  public static void main(String[] argv) {
    Object implementor = new Calculator();
    String address = "http://localhost:9000/Calculator";
    Endpoint.publish(address, implementor);
  }
}
